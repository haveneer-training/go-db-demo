FROM golang:alpine AS golang

# git is required foir go get
RUN apk --no-cache add git
RUN apk --no-cache add gcc libc-dev

WORKDIR /go/src/app
COPY . .

ENV GOPATH /go

# Static build without debugging infos.
# cf `go tool link`
# -s	                disable symbol table : prevent from using 'go tool nm'
# -w	                disable DWARF generation : remove debugging metadata (prevent from using gdb or pprof tools)
# -extldflags -static   for static link (no more needs ld-musl)
RUN go install -ldflags "-w -s -linkmode external -extldflags -static"

FROM scratch
# debug purpose avec alpine
#RUN apk --no-cache add strace binutils file

COPY --from=golang /go/bin/go-db-demo /app
COPY ./template.html /

WORKDIR /

# This container exposes port 3000 to the outside world
EXPOSE 3000

ENTRYPOINT ["/app"]
CMD ["-h"]
