## Configuration de la base de données 
La base de données doit être de type PostgreSQL et initialisée avec le schéma présent dans `initdb.d/init.sql`

Pour l'exemple,
* le service de base de données sera accessible par le port 5432 de localhost
* l'utilisateur ayant accès doit être `postgres`
* son mot de passe doit être enregistré dans le fichier `./password.dat`
* le nom de la base de données sera `postgres`

Une image docker de PostgreSQL est disponible sur Docker Hub : https://hub.docker.com/_/postgres

Les principales variables d'environnement à définir sont                                                               
* `POSTGRES_USER`
* `POSTGRES_PASSWORD`
* `POSTGRES_DB`

Exemple de lancement
```bash
docker run -d \
  -v "$PWD"/initdb.d:/docker-entrypoint-initdb.d \
  -v "$PWD"/password.dat:/password.dat \
  -e POSTGRES_PASSWORD_FILE=/password.dat \
  -p 5432:5432 --name postgres postgres
```

## Initialiation des dépendances

```bash
go get -u github.com/lib/pq
```

## Construction
```bash
go build .
```
 
## Première exécution et aide d'utilisation
```bash
./go-db-demo -h
```

## Construction du container
```bash
docker build -t registry.gitlab.com/haveneer-training/go-db-demo .
```

## Lancement du container pour aide d'utilisation  
```bash
docker run -it --rm \
  -v "$PWD"/password.dat:/password.dat \
  registry.gitlab.com/haveneer-training/go-db-demo:latest
```

Output
```text
Usage of /app:
  -dbhost string
    	DB host (default "localhost")
  -dbname string
    	DB name (default "postgres")
  -dbport int
    	DB dbport (default 5432)
  -password string
    	DB password
  -password-as-secret
    	Consider password as a secret handler
  -user string
    	DB user (default "postgres")
```

## Lancement du container 
```bash
docker run -it --rm \
  --network="bridge" \
  --publish 8080:3000 \
  -v "$PWD"/password.dat:/password.dat \
  registry.gitlab.com/haveneer-training/go-db-demo:latest \
    -password ./password.dat -password-as-secret -dbhost ${IP}
```

avec `$IP` défini
* sur MacOS : 
```bash
IP=$(route -n get default|grep interface|awk '{ print $2 }'|xargs ipconfig getifaddr)
```
ou bien
```bash
IP=$(ifconfig | grep "inet " | grep -v 127.0.0.1 | cut -d\  -f2 | head -n1)
```

* sur Linux
```bash
IP=$(/sbin/route | grep "default" | awk '{ print $8 }' | xargs ip -4 addr show | grep -oE '[0-9]+(\.[0-9]+){3}' | head -n1)
```
ou bien
```bash
IP=$(ip -4 addr show | grep -oE '[0-9]+(\.[0-9]+){3}' | grep -v 127.0.0.1 | head -n1)
```
