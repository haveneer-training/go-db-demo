-- Table des évènements
CREATE TABLE events (
  id SERIAL PRIMARY KEY,
  date TIMESTAMP NOT NULL DEFAULT NOW(),
  context CHARACTER VARYING
);