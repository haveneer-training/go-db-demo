#!/bin/sh
set -e

at_term() {
    echo 'Sleep terminated.'
    exit 0
}

trap at_term TERM INT

echo 'Sleeping...'
(while true; do sleep 3600; done) &
wait
