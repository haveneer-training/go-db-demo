# version de docker-compose
version: "3.3"

# définition des services
services:
  # service de base données (nom personnalisé)
  postgres:
    # nom de l'image requise
    image: postgres:${POSTGRES_VERSION}
    # définition de variables d'environnement pour l'exécution du container
    # (utilisant la configuration .env soit directement soit par interpolation)
    environment:
      - POSTGRES_USER=${POSTGRES_USER}
      - POSTGRES_PASSWORD_FILE=${PASSWORD_FILE}
      - POSTGRES_DB=${POSTGRES_DB}
    # Montage de volumes extérieurs
    volumes:
      # répertoire local (commence par un . ou un /)
      - './initdb.d:/docker-entrypoint-initdb.d'

      # fichier local en lecture seule
      # - ${PASSWORD_FILE}:/password.dat:ro
      # prefer long syntax to avoid implicit directory creation if not existing
      - type: bind
        source: ${PASSWORD_FILE}
        target: /password.dat
        read_only: true

      # volume géré par docker (en référence à la liste des volumes ci-en bas)
      - 'postgresql_data:/var/lib/postgresql/data'
    # connexion au réseau interne (en référence à la liste des réseaux ci-en bas)
    networks:
      - dbnet

  # le service ouvert à l'extérieur (nom personnalisé)
  web:
    # nom de l'image requise
    # sert aussi de tag pour la reconstruction
    image: 'registry.gitlab.com/haveneer-training/go-db-demo:latest'
    # demande un redémarrage systématique du service s'il devait tomber
    restart: "always"
    # commande alternative à celle par défaut
    # cette version décompose en arguments suivant les séparateurs conventionnels
    # multiline value (starting with pipe)
    command: |
        -dbhost db
        -dbname ${POSTGRES_DB}
        -retry ${RETRY_COUNT}
        -password ${PASSWORD_FILE}
        -password-as-secret
    # méthode de (re)construction de l'image
    build:
      # le répertoire de reconstruction
      context: ..
      # le nom du fichier de la recette
      dockerfile: Dockerfile
    # Montage de volume extérieur (en lecture seule)
    volumes:
      # same as in postgres service
      - type: bind
        source: ${PASSWORD_FILE}
        target: /password.dat
        read_only: true
    # redirection des ports
    ports:
      - "8080:3000"
    # surcharge du répertoire de lancement
    working_dir: /
    # La dépendance n'implique qu'un ordre de lancement pas la pleine disponibilité des services requis
    depends_on:
      - postgres
    # alias réseau
    links:
      - postgres:db
    # connexion aux réseaux internes (en référence à la liste des réseaux ci-en bas)
    # ATTN: syntaxe dictionnaire JSON like (précédemment c'est list like)
    networks:
      dbnet:
      webnet:
        # définit des alias pour ce container sur le réseau webnet
        aliases:
          - theweb

  # service de contrôle (nom personnalisé)
  healthcheck:
    # nom de l'image requise
    # sert aussi de tag pour la reconstruction
    image: 'alpine-curl'
    # demande un redémarrage systématique du service s'il devait tomber
    restart: "always"
    # version synthétique de la reconstruction (suppose des paramètres par défaut)
    build: ./alpine-curl
    # commande de vérification de santé
    healthcheck:
      # commande exprimée de l'intérieur du sous-réseau de la composition (via l'alias dans webnet)
      test: curl -sS "http://theweb:3000/healthcheck" -o /dev/null
      # délai entre deux contrôles
      interval: 60s
    # La dépendance n'implique qu'un ordre de lancement pas la pleine disponibilité des services requis
    depends_on:
      - web
    # connexion au réseau interne (en référence à la liste des réseaux ci-en bas)
    networks:
      - webnet
    # commande alternative à celle par défaut
    # la version en tableau définit précisément la décomposition en arguments
    command: "/infinite-sleep.sh"
    # command: ["/bin/sh", "-c", "trap : TERM INT; (while true; do sleep 3600; done) & wait"]
    # Demande l'activation spécifique d'une capacité du noyau (ici pour debug avec strace)
    cap_add:
      - SYS_PTRACE

# Liste des réseaux internes
networks:
  # (nom personnalisés)
  webnet:
  dbnet:

# Liste des volumes internes
volumes:
  # (nom personnalisés)
  postgresql_data:
    # paramétrisation du type de stockage
    # (facultatif; ici avec sa valeur par défaut)
    driver: local
    # pourrait permettre de référencer un volume créer à l'extérieur
    # (facultatif; ici avec sa valeur par défaut)
    external: false

