#!/bin/bash
set -e

set -x
docker-compose up -d

sleep 10

docker-compose logs --tail 10

sleep 10

docker-compose build --no-cache healthcheck

sleep 10

docker-compose up -d

sleep 10

curl -IsS "http://localhost:8080/demo-check"

sleep 10

docker-compose logs --tail 10

sleep 10

docker-compose down

sleep 10
