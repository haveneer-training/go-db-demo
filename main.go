package main

// Inspiré de : https://www.calhoun.io/connecting-to-a-postgresql-database-with-gos-database-sql-package/

import (
	"bytes"
	"database/sql"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"path"
	"strconv"

	// https://www.calhoun.io/why-we-import-sql-drivers-with-the-blank-identifier/
	// https://stackoverflow.com/questions/21220077/what-does-an-underscore-in-front-of-an-import-statement-mean
	_ "github.com/lib/pq"

	"time"
)

// Ce sont des pointeurs remplis avec la funtion flag.Parse
var dbport *int = flag.Int("dbport", 5432, "DB dbport")
var dbhost *string = flag.String("dbhost", "localhost", "DB host")
var dbname *string = flag.String("dbname", "postgres", "DB name")
var retry *int = flag.Int("retry", 0, "Retrial count before giving up\n(<0 means infinite)")
var user *string = flag.String("user", "postgres", "DB user")
var password *string = flag.String("password", "", "DB password")
var port *int = flag.Int("port", 3000, "The port which will be served.")

// Explained format : https://golang.org/pkg/fmt/
const timeFormat = "2006-01-02 at 15:04:05.000 (MST)"

func init() {
	var useSecret bool = false
	flag.BoolVar(&useSecret, "password-as-secret", false, "Consider password as a secret handler")

	// option parsing
	flag.Parse()

	if useSecret {
		if dat, err := ioutil.ReadFile(*password); err != nil {
			panic(err)
		} else {
			*password = string(dat)
		}
	}
}

func main() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		*dbhost, *dbport, *user, *password, *dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close() // todo: https://pocketgophers.com/handling-errors-in-defer/

	for true {
		err = db.Ping()
		if err == nil {
			break
		} else {
			if *retry == 0 {
				panic(err)
			}
			log.Println("Trying DB connection:", err)
			if *retry > 0 {
				*retry--
			}
			time.Sleep(1 * time.Second)
		}
	}

	log.Printf("DB %s:%d/%s successfully connected!", *dbhost, *dbport, *dbname)

	log.Printf("Serving on port %v\n", *port)
	http.HandleFunc("/",
		func(w http.ResponseWriter, r *http.Request) {
			myHandler(w, r, db)
		})

	err = http.ListenAndServe(":"+strconv.Itoa(*port), nil) // set listen port
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
	log.Printf("Server is up and running")
}

type Event struct {
	Id      int
	Date    time.Time
	Context string
}

func myHandler(w http.ResponseWriter, r *http.Request, db *sql.DB) {
	log.Printf("Receive %s request %s\n", r.Method, r.URL.Path)
	//log.Printf("\tdetails: %#v\n", r)

	const sqlInsert = `INSERT INTO events (context) VALUES ($1) RETURNING id`
	id := 0
	context := fmt.Sprintf("%s has requested %s", r.RemoteAddr, r.RequestURI)
	err := db.QueryRow(sqlInsert, context).Scan(&id)
	if err != nil {
		panic(err)
	}
	log.Println("New record ID is:", id)

	// `curl -i http://localhost:3000/kill` to throw a fatal error
	if r.RequestURI == "/kill" {
		log.Fatalln("kill requested")
	}

	const sqlSelect = `SELECT id, date, context FROM events ORDER BY id DESC LIMIT $1`
	rows, err := db.Query(sqlSelect, 10)
	if err != nil {
		// handle this error better than this
		panic(err)
	}
	defer rows.Close() // todo: https://pocketgophers.com/handling-errors-in-defer/

	var events []Event

	for rows.Next() {
		event := Event{}
		err = rows.Scan(&event.Id, &event.Date, &event.Context)
		if err != nil {
			// handle this error
			panic(err)
		}
		events = append(events, event)
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "text/html")

	fmap := template.FuncMap{
		"formatAsDate": formatAsDate,
	}

	// https://golang.org/pkg/text/template/
	// https://stackoverflow.com/questions/49043292/error-template-is-an-incomplete-or-empty-template
	const templateFile = "./template.html"
	templateName := path.Base(templateFile)
	tmpl := template.Must(template.New(templateName).Funcs(fmap).ParseFiles(templateFile))

	// use an intermediate buffer to switch to an error status if an internal error is detected
	buffer := new(bytes.Buffer)
	if err := tmpl.ExecuteTemplate(buffer, templateName, events); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		if _, err = fmt.Fprintln(w, "500: Internal error"); err != nil {
			log.Panic(err)
		}
		log.Println("Cannot write template:", err)
	} else {
		if _, err = w.Write(buffer.Bytes()); err != nil {
			log.Println("Error while writing buffer")
		}
	}
}

func formatAsDate(t time.Time) string {
	return fmt.Sprintf(t.Format(timeFormat))
}
